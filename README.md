
# Left 4 Dead 2 ProMod Dockerfile

On Windows, make sure your Docker machine has enough space:

```
docker-machine rm default
docker-machine create -d virtualbox --virtualbox-disk-size "20000" default
```

Clone this repository, including sub-modules:

```
git clone https://gitlab.com/jojolebarjos/l4d2-promod-docker.git
cd l4d2-promod-docker
git submodule init
git submodule update
```

Build the image using:

```
docker build -t jojolebarjos/l4d2 .
```

Once built, you might want to export the image for posterity:

```
docker save -o l4d2.tar jojolebarjos/l4d2
```

Run the server using:

```
docker run \
    -p 27000-27015:27000-27015/udp \
    -p 27015:27015 \
    --name l4d2 \
    -ti \
    jojolebarjos/l4d2 \
    -tickrate 60 \
    +sv_lan 1 \
    +hostname "U-WUT server" \
    +sv_region 3 \
    +map c8m1_apartment
```


## Server-side FAQ


### How to start a match?

When players join the game, they can vote for a new match by saying `!match` in the public chat.


### What is "ProMod U-WUT"?

This is our custom variant of ProMod. See [this repository](https://bitbucket.org/jojolebarjos/uwut) for more information.


### How to set up admins?

See this [wiki](https://wiki.alliedmods.net/Adding_Admins_(SourceMod)). You will probably need to find your [steam ID](https://steamidfinder.com/). Note that you will need to rebuild the Docker image if you update files.


### How to forward ports?

On Windows, make sure you also forward your VirtualBox ports. To do that, open _Oracle VM VirtualBox_, open the settings of your Docker machine (e.g. `default`) and go to Network > Adapter 1. You can either:

 * Keep using NAT and forward each port manually, or
 * Choose Bridged Adapter to expose your Docker machine as a separate IP address on your local network.

You will also need to setup your router to forward connections to your machine, if you are behind a NAT.


## Client-side FAQ


### How to properly configure client?

We suggest two modifications to your `left4dead2/cfg/autoexec.cfg`, as described in our `autoexec.cfg`:

 1. We adjust some network configuration, which includes `cl_updaterate` to actually increase the tick rate.
 2. We add a network debug graph when scoreboard is diplayed.


### How to change video settings client-side?

In case you made weird changes and cannot update video settings anymore, or if you just don't find suitable properties in the drop-down, edit `left4dead2/cfg/video.txt`. You can use `left4dead2/cfg/videodefaults.txt` for reference. You might want to check these:

```
    "setting.defaultres"        "1920"
    "setting.defaultresheight"  "1080"
    "setting.fullscreen"        "0"
    "setting.nowindowborder"    "1"
```


## Links

 * https://github.com/InAnimaTe/docker-steamcmd-play
 * https://github.com/Stabbath/ProMod
 * https://github.com/Attano/L4D2-Competitive-Framework
 * https://github.com/SirPlease/L4D2-Competitive-Rework
