#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>


public Plugin:myinfo =
{
    name = "Tank Home Run",
    author = "Jojo le Barjos",
    description = "Tank can throw its special friends by hitting them hard",
    version = "0.2",
    url = ""
};

new Handle:hEnabled;
new Handle:hThrowDamage;
new Handle:hThrowVelocity;
new Handle:hAllowRock;

new bool:bLateLoad;


public APLRes:AskPluginLoad2(Handle:plugin, bool:late, String:error[], errMax) 
{
    bLateLoad = late;
    return APLRes_Success;
}

public OnPluginStart()
{
    hEnabled = CreateConVar("tank_home_run_enabled", "1", "Whether tank home run is active.");
    hThrowDamage = CreateConVar("tank_home_run_damage", "1.0", "Damage done by tank to SI (negative to leave as-is).");
    hThrowVelocity = CreateConVar("tank_home_run_velocity", "1000.0", "Velocity at which SI are thrown when punched by the tank.");
    hAllowRock = CreateConVar("tank_home_run_allow_rock", "1", "Whether throwing a rock at a SI also counts.");

    if (bLateLoad) 
    {
        for (new i = 1; i <= MaxClients; i++) 
        {
            if (IsClientInGame(i)) 
            {
                OnClientPutInServer(i);
            }
        }
    }
}

public OnClientPutInServer(client)
{
    SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}

public OnClientDisconnect(client)
{
    SDKUnhook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}

public Action:OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damageType, &weapon, Float:damageForce[3], Float:damagePosition[3]) 
{
    if (!GetConVarBool(hEnabled))
        return Plugin_Continue;

    if (!IsInfected(victim))
        return Plugin_Continue;

    if (!IsTank(attacker))
        return Plugin_Continue;

    if (!GetConVarBool(hAllowRock) && IsTankRock(inflictor))
        return Plugin_Continue;

    PrintToServer("Tank did '%f' damages with force [%f, %f, %f]", damage, damageForce[0], damageForce[1], damageForce[2]);

    decl Float:desiredDamage;
    desiredDamage = GetConVarFloat(hThrowDamage);
    if (desiredDamage >= 0.0)
        damage = desiredDamage;

    decl Float:velocity[3];
    velocity[0] = damageForce[0];
    velocity[1] = damageForce[1];
    velocity[2] = damageForce[2];
    NormalizeVector(velocity, velocity);

    velocity[2] += 0.2;
    NormalizeVector(velocity, velocity);

    ScaleVector(velocity, GetConVarFloat(hThrowVelocity));

    TeleportEntity(victim, NULL_VECTOR, NULL_VECTOR, velocity);

    return Plugin_Changed;
}

bool:IsInfected(client)
{
    return client > 0 && client <= MaxClients && IsClientInGame(client) && GetClientTeam(client) == 3;
}

bool:IsTank(client)
{
    return IsInfected(client) && GetEntProp(client, Prop_Send, "m_zombieClass") == 8 && IsPlayerAlive(client);
}

bool:IsTankRock(entity)
{
    if (entity > 0 && IsValidEntity(entity) && IsValidEdict(entity))
    {
        decl String:classname[64];
        GetEdictClassname(entity, classname, sizeof(classname));
        return StrEqual(classname, "tank_rock");
    }
    return false;
}
