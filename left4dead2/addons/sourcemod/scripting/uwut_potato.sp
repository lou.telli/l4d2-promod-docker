#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>


public Plugin:myinfo =
{
    name = "Potato gun",
    author = "Jojo le Barjos",
    description = "Everybody get a potato gun",
    version = "0.2",
    url = ""
};

new Handle:hPotatoCount;

new bool:bNewRound;


public void OnPluginStart()
{
    hPotatoCount = CreateConVar("potato_count", "4", "How many grenade launcher are spawned.");
    bNewRound = false;
    CreateTimer(2.0, Timer_SpawnItems, INVALID_HANDLE, TIMER_REPEAT);
    HookEvent("round_start", Event_RoundStart);
}

public Action:Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
    bNewRound = true;
    return Plugin_Continue;
}

public Action:Timer_SpawnItems(Handle:timer)
{
    decl Float:location[3];
    if (bNewRound && GetNextLocation(location))
    {
        PrintToServer("Potato gun spawner has detected round start");
        bNewRound = false;
        SpawnItems();
    }
    
    for (new i = 1; i <= MaxClients; i++) {
    
    }
    //SetEntProp(replacement, Prop_Data, "m_iClip1", GetConVarInt(M60AmmoCVAR), 1);
}

static SpawnItems()
{
    decl count;
    count = GetConVarInt(hPotatoCount);
    PrintToServer("Spawning %d potato guns...", count);
    for (new i = 0; i < count; ++i)
        SpawnPotatoGun();
}

static bool:SpawnPotatoGun()
{
    decl Float:location[3];
    if (!GetNextLocation(location))
    {
        PrintToServer("  Failed to find location for potato gun!");
        return false;
    }

    if (!SpawnPotatoGunAt(location))
    {
        PrintToServer("  Failed to spawn potato gun at (%f, %f, %f)", location[0], location[1], location[2]);
        return false;
    }

    PrintToServer("  Successfully spawned potato gun at (%f, %f, %f)", location[0], location[1], location[2]);
    return true;
}

static bool:SpawnPotatoGunAt(const Float:location[3])
{
    new entity = CreateEntityByName("weapon_grenade_launcher");
    if (entity < 0)
        return false;

    DispatchKeyValue(entity, "solid", "6");
    DispatchSpawn(entity);

    TeleportEntity(entity, location, NULL_VECTOR, NULL_VECTOR);
    SetEntProp(entity, Prop_Send, "m_iExtraPrimaryAmmo", 30, 4);

    ActivateEntity(entity);

    return true;
}

static bool:GetNextLocation(Float:location[3])
{
    new client = GetNextSurvivor();
    if (client == 0)
        return false;
    GetClientAbsOrigin(client, location);
    location[0] += GetURandomFloat() * 8 - 4;
    location[1] += GetURandomFloat() * 8 - 4;
    location[2] += GetURandomFloat() * 8 + 22;
    return true;
}

static GetNextSurvivor()
{
    static old = 0;
    for (new i = 1; i <= MaxClients; i++) {
        new client = old + i;
        while (client > MaxClients)
            client -= MaxClients;
        if (IsClientConnected(client) && IsClientInGame(client) && IsPlayerAlive(client) && GetClientTeam(client) == 2) {
            old = client;
            return client;
        }
    }
    old = 0;
    return 0;
}
